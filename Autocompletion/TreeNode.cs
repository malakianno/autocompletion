﻿using System;
using System.Collections;

namespace Autocompletion
{
    struct TreeNode
    {
        private string word; // Накопленное слово при просмотре дерева "сверху-вниз".
        private int popularity; // Популярность слова word.
        private TreeNode[] children; // Дочерние записи.
        private const int maxChildren = 3; // Начальное максимальное кол-во дочерних записей (после превышения порога, массив дочерних записей динамически изменяется).
        private int countNotNullChildren; // Кол-во ненулевых дочерних записей.

        public TreeNode(string word)
        {
            this.word = word;
            popularity = 0;
            children = new TreeNode[maxChildren];
            countNotNullChildren = 0;
        }

        // Буква записи
        public char Letter
        {
            get
            {
                return word[word.Length - 1]; // Последняя буква накопленного слова при просмотре дерева "сверху-вниз"
            }
        }

        // Добавляет слово word с популярностью popularity в текущее дерево, numOfCurrentLetter обозначает текущую позицию в word
        public void AddWord(string word, int popularity, int numOfCurrentLetter)
        {
            // Если текущая буква последняя, то завершаем построение слова в дереве
            if (this.word.Length == word.Length)
            {
                this.popularity = popularity;
                return;
            }
            else
            {
                // Иначе среди дочерних записей ищем запись со следующей буквой
                bool is_found = false;
                for (int i = 0; i < countNotNullChildren; i++)
                    if (children[i].Letter == word[numOfCurrentLetter + 1])
                    {
                        is_found = true;
                        children[i].AddWord(word, popularity, numOfCurrentLetter + 1);
                        break;
                    }
                // Если не нашлось записи с такой буквой, то создаем дочернюю запись с такой буквой
                if (!is_found)
                {
                    // Если необходимо увеличить размер массива
                    if (countNotNullChildren >= maxChildren)
                        Array.Resize(ref children, children.Length + 1);

                    // Помещаем добавляемую запись (содержащую очередную букву) с учетом убывания популярности букв дочерних записей
                    int index = 0;
                    bool isLocated = false;
                    for (int i = 0; i < countNotNullChildren; i++)
                        if (children[i].popularity < popularity)
                        {
                            isLocated = true;
                            for (int j = countNotNullChildren; j > i; j--)
                                children[j] = children[j - 1];
                            index = i;
                            break;
                        }
                    if (!isLocated)
                        index = countNotNullChildren;
                    children[index] = new TreeNode(word.Substring(0, numOfCurrentLetter + 2));
                    children[index].AddWord(word, popularity, numOfCurrentLetter + 1);
                    countNotNullChildren++;
                }
            }
        }

        // Ищет слова, дополняющие слово word и выводит их в buffer; при этом результаты запросов хэшируются в autocompletionForWords,
        // numOfCurrentLetter обозначает текущую позицию в запросе word при перемещении по дереву записей с буквами.
        public void SearchWords(string word, int numOfCurrentLetter, Hashtable autocompletionForWords, Buffer buffer)
        {
            // Спускаемся по дереву до уровня, соответствующего последней букве запроса и выводим все слова лежащие ниже
            if (this.word.Length == word.Length)
                OutputWords(ref buffer);
            else
            {
                for (int i = 0; i < countNotNullChildren; i++)
                    if (children[i].Letter == word[numOfCurrentLetter + 1])
                    {
                        children[i].SearchWords(word, numOfCurrentLetter + 1, autocompletionForWords, buffer);
                        break;
                    }
            }
        }

        // Выводит все слова начиная с текущей записи
        private void OutputWords(ref Buffer buffer)
        {
            if (popularity != 0)
                buffer.Add(word, popularity);

            for (int i = 0; i < countNotNullChildren; i++)
                children[i].OutputWords(ref buffer);
        }
    }
}