﻿using System;
using System.Collections;

namespace Autocompletion
{
    class Program
    {
        static void Main()
        {
            TreeNode[] roots = new TreeNode[0];
            // Чтение словаря
            int dictionarySize = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < dictionarySize; i++)
            {
                string[] splitted = Console.ReadLine().Split(' ');
                // Ищем дерево, в корне которого первая буква, прочитанного слова
                int indexRoot = 0;
                bool isRootFound = false;
                for (int j = 0; j < roots.Length; j++)
                    if (roots[j].Letter == splitted[0][0])
                    {
                        isRootFound = true;
                        indexRoot = j;
                        break;
                    }

                // Если такое дерево не найдено, создаем его
                if (!isRootFound)
                {
                    Array.Resize(ref roots, roots.Length + 1);
                    indexRoot = roots.Length - 1;
                    roots[indexRoot] = new TreeNode(splitted[0][0].ToString());
                }

                // Добавляем прочитанное слово в дерево
                roots[indexRoot].AddWord(splitted[0], Convert.ToInt32(splitted[1]), 0);
            }

            // Чтение запросов
            int wordsCount = Convert.ToInt32(Console.ReadLine());
            string[] words = new string[wordsCount];
            for (int i = 0; i < wordsCount; i++)
                words[i] = Console.ReadLine();

            // Поиск слов и вывод результатов.
            Buffer buffer = new Buffer();
            Hashtable autocompletionForWords = new Hashtable();
            for (int i = 0; i < wordsCount; i++)
            {
                bool isFound = false;
                if (autocompletionForWords.ContainsKey(words[i])) // Ищем в хэш-таблице
                {
                    buffer.LoadData((string[])autocompletionForWords[words[i]]);
                    isFound = true;
                }
                else // ищем в дереве
                    for (int j = 0; j < roots.Length; j++)
                        if (roots[j].Letter == words[i][0])
                        {
                            roots[j].SearchWords(words[i], 0, autocompletionForWords, buffer);
                            autocompletionForWords.Add(words[i], buffer.GetCopyData());
                            isFound = true;
                            break;
                        }
                if (isFound)
                {
                    buffer.PrintToConsole();
                    buffer.Reset();
                }
                Console.WriteLine();
            }
        }
    }
}