﻿using System;

namespace Autocompletion
{
    class Buffer
    {
        private string[] words; // Слова буффера (автодополнения).
        private int[] popularity; // Популярностью соответствующих слов
        private int count; // Кол-во ненулевых элементов.
        private const int maxSize = 10; // Размер буфера.

        // Возвращает количество ненулевых элементов буфера
        public int Count
        {
            get
            {
                return count;
            }
        }

        public Buffer()
        {
            words = new string[maxSize];
            popularity = new int[maxSize];
            count = 0;
        }

        // Добавляет новое слово newWord с популярностью newPopularity
        public void Add(string newWord, int newPopularity)
        {
            if (count == maxSize)
            {
                // Если популярность добавляемого варианта автодополнения ниже популярности самого непопулярного слова
                // в буффере, тогда не добавляем его.
                if (newPopularity < popularity[maxSize - 1])
                    return;
            }

            // Ищем слово с такой же популярностью, перед которым должно стоять добавляемое слово
            bool found = false;
            for (int i = 0; i < count; i++)
                if (newPopularity == popularity[i] && newWord.CompareTo(words[i]) == -1)
                {
                    found = true;
                    for (int j = (count == maxSize) ? count - 1 : count; j > i; j--)
                    {
                        words[j] = words[j - 1];
                        popularity[j] = popularity[j - 1];
                    }
                    words[i] = newWord;
                    popularity[i] = newPopularity;
                    if (count != maxSize)
                        count++;
                    break;
                }
            if (!found)
            {
                int max = (count == maxSize) ? count : count + 1;
                for (int i = 0; i < max; i++)
                    if (newPopularity > popularity[i])
                    {
                        for (int j = (count == maxSize) ? count - 1 : count; j > i; j--)
                        {
                            words[j] = words[j - 1];
                            popularity[j] = popularity[j - 1];
                        }
                        words[i] = newWord;
                        popularity[i] = newPopularity;
                        if (count != maxSize)
                            count++;
                        break;
                    }
            }
        }

        // Выводит содрежимое в консоль
        public void PrintToConsole()
        {
            for (int i = 0; i < count; i++)
                Console.WriteLine(words[i]);
        }

        // Сброс
        public void Reset()
        {
            count = 0;
            for (int i = 0; i < maxSize; i++)
            {
                popularity[i] = 0;
                words[i] = null;
            }
        }

        // Записывает содержимое в поток output
        public void PrintToFile(System.IO.StreamWriter output)
        {
            for (int i = 0; i < count; i++)
                output.WriteLine(words[i]);
        }

        // Возвращает копию содержимого
        public string[] GetCopyData()
        {
            string[] copy = new string[maxSize];
            words.CopyTo(copy, 0);
            return copy;
        }

        // Загружает данные
        public void LoadData(string[] words)
        {

            int countNotNull = 0;
            for (int i = 0; i < words.Length && words[i] != null; i++)
            {
                this.words[i] = words[i];
                countNotNull++;
            }
            count = countNotNull;
        }
    }
}